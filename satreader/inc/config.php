<?php

/**
 * Configuration file for inc repository
 *
 * @author Nasrul H. Mohamad <nasrul_hazim@live.com>
 * @version 1.0
 * @package inc
 */

/* Turn on Error & Warning reporting except Notice */
error_reporting(E_ALL ^ E_NOTICE);

/* @link http://jonraasch.com/blog/5-asset-management-tricks-for-faster-websites */
ob_start("ob_gzhandler");
define(DIR_SEPARATOR,'/');

/* Define global directory name */
define(DIR_GLOBAL_NAME, 'global');

/* Define include directory path */
define(DIR_INC, str_replace('\\',DIR_SEPARATOR,dirname(__FILE__)));

/* Define global directory path. Go up one level, must be outside of inc directory, and same level as inc directory */
define(DIR_GLOBAL, str_replace('\\',DIR_SEPARATOR,dirname(dirname(__FILE__))) . DIR_SEPARATOR . DIR_GLOBAL_NAME);

/* Database Manager using adodb5 */
include_once DIR_INC . DIR_SEPARATOR . 'FileInclude.php'; 

$list = array();

$list[] = 'DBConnector';/* Database Manager using adodb5 */
$list[] = 'DBConf';/* Database Configuration */
$list[] = 'StatConverter';/* Database Configuration */

FileInclude::includeFiles($list);

?>